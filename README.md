# Unofficial Netlify CLI Docker image

This repository contains a Dockerfile that produces a Docker image for Netlify CLI that can be used in Gitlab CI pipelines.

## Download

The Docker image is hosted in [Gitlab container registry](https://gitlab.com/lepovirta/netlify-cli/container_registry).
You can download it, for example, using Docker.

```bash
docker pull registry.gitlab.com/lepovirta/netlify-cli
```

## LICENSE

MIT license. See [LICENSE](LICENSE) for more information.
