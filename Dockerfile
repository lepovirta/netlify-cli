FROM node:14-slim

ENV DEBIAN_FRONTEND=noninteractive

# Support packages
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        ca-certificates \
        curl \
        git \
    && rm -rf /var/lib/apt/lists/*

# Netlify CLI
RUN npm install -g netlify-cli

# Non-root user
WORKDIR /project
RUN groupadd -g 10101 netlify \
    && useradd -u 10101 -g netlify -M -d /project netlify \
    && chown -R netlify:netlify /project
USER netlify:netlify
